#include "bubble_sort.h"
#include "insertion_sort.h"
#include "merge_sort.h"
#include "quick_sort.h"
#include "selection_sort.h"
#include <stdio.h>

int main(int argc, char const *argv[])
{
	/* code */
	int array[10] = {5, 7, 10, 3, 2, 1, 16, 11, 6, 20};
	int size = sizeof(array)/sizeof(array[0]);
    signed op;

    printf("Messy array\n");

    for (int i = 0; i < size; ++i){
		printf("%d ", array[i]);
	}

    printf("\n\nSelect: \n\n 1 - Bubble Sort\n\n 2 - Insertion Sort\n\n 3 - Merge Sort\n\n 4 - Quick Sort\n\n 5 - Selection Sort\n\n Op:");
    scanf("%d",&op);

    switch(op){
        case 1:
            bubbleSort(array, size);
            break;
        case 2:
            insertionSort(array, size);
            break;
        case 3:
            mergeSort(array, size);
            break;
        case 4:
            quickSort(array, size);
            break;
        case 5:
            selectionSort(array, size);
            break;                
    }

    printf("\n Sorted\n");

	for (int i = 0; i < size; ++i){
		printf("%d ", array[i]);
	}

	return 0;
}