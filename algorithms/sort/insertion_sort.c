/*
Algoritmo de ordenação: Insertion Sort
Complexidade do tempo: O(n²)
Descrição: Algoritmo de ordenação simples que funciona da maneira como ordenamos cartas de jogar em nossas mãos. 
Ordenamos as duas primeiras cartas e depois colocamos a terceira carta na posição apropriada dentro das duas primeiras, 
e depois a quarta está posicionada dentro das três primeiras e assim por diante até que a mão inteira seja orndenada.
*/

#include <stdio.h>
#include "swap.h"
#include "insertion_sort.h"

void insertionSort(int * array, int size){
	int j;
    for(int i = 1; i < size; i++){
        j = i;
        while(j > 0 && (array[j-1] > array[j])){
            swap(array, j, j-1);
            j--;
        }   
    }
}
