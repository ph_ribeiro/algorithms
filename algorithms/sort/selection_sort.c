/*
Algoritmo de ordenação: Selection Sort
Complexidade do tempo: O(n²)
Descrição: Algoritmo de ordenação simples que encontra o menor elemento na matriz e troca-o com o elemento na primeira posição, 
depois encontra o segundo menor elemento e o troca com o elemento na segunda posição, e assim continua até a matriz inteira esteja ordenada.
*/

#include <stdio.h>
#include "swap.h"
#include "selection_sort.h"

void selectionSort(int * array, int size){
	int min;
    for(int i = 0; i < size-1; i++){
        min = i;
        for(size_t j = i+1; j < size; j++){       
            if(array[j] < array[min]){      
                min = j;
            }
        }   
        swap(array, min, i);
    }
}
