#ifndef MERGE_SORT_H_
#define MERGE_SORT_H_

int mergeHalves(int * array, int leftStart, int rightEnd, int middle);

void mergeSort_core(int * array, int left, int right);

void mergeSort(int * array, int size);

#endif