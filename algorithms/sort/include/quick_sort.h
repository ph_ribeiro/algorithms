#ifndef QUICK_SORT_H_
#define QUICK_SORT_H_

int partition(int * array, int left, int right, int pivo);

void quickSort_core(int * array, int left, int right);

void quickSort(int * array, int size);

#endif